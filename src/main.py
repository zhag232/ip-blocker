import os
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import parse_qs
import psycopg2
from datetime import datetime, timezone
import sys
import smtplib
from email.message import EmailMessage


class MyServer(BaseHTTPRequestHandler):
    def sql_insert(self, ip, path):
        dt = datetime.now(timezone.utc)
        try:
            cur.execute('INSERT INTO IPBLOCKED (DATE,IP,PATH) VALUES (%s, %s, %s)', (dt, ip, path))
            con.commit()
            print("Record inserted successfully")
        except:
            print("Failed to inserted record")
            sys.exit(1)

    def smtp_send(self, ip, path):
        dt = datetime.now(timezone.utc)
        try:
            msg = EmailMessage()
            content = "This IP address has been blocked: " + ip + '\n' + "Date: " + str(dt) + '\n' + 'Path: ' + path
            msg.set_content(content)
            msg["From"] = emailfrom
            msg["To"] = emailto
            msg['Subject'] = "IP has been blocked"
            s = smtplib.SMTP(host=smtpserver, port=smtpport)
            s.send_message(msg)
            s.quit()
        except:
            print("Failed to send email to: " + emailto)

    def do_GET(self):
        path = self.path
        x_real_ip = self.headers.get("X-Real-IP")
        x_forwarded_for = self.headers.get("X-Forwarded-For")
        if x_real_ip is not None:
            ip = x_real_ip
        elif x_forwarded_for is not None:
            ip = x_forwarded_for
        else:
            ip = self.client_address[0]
        if ip in blacklist:
            self.send_response(444)
            self.end_headers()
        else:
            if path == "/blacklisted" or path == "/blacklisted/":
                self.send_response(444)
                self.end_headers()
                self.sql_insert(ip, path)
                blacklist.append(ip)
                self.smtp_send(ip, path)
            elif '/?' in path:
                try:
                    params = parse_qs(path[2:])
                    n = params['n'][0]
                    result = float(n) ** 2
                    self.send_response(200)
                    self.send_header("Content-type", "text/html")
                    self.end_headers()
                    self.wfile.write(bytes("<html><head><title>ip-blocker</title></head>", "utf-8"))
                    self.wfile.write(bytes("<p>%s</p>" % result, "utf-8"))
                    self.wfile.write(bytes("<body>", "utf-8"))
                    self.wfile.write(bytes("</body></html>", "utf-8"))
                except:
                    self.send_response(500)
                    self.send_header("Content-type", "text/html")
                    self.end_headers()
                    self.wfile.write(bytes("<html><head><title>ip-blocker</title></head>", "utf-8"))
                    self.wfile.write(bytes("<p>500 Internal Server Error</p>", "utf-8"))
                    self.wfile.write(bytes("<body>", "utf-8"))
                    self.wfile.write(bytes("</body></html>", "utf-8"))
            else:
                self.send_response(404)
                self.send_header("Content-type", "text/html")
                self.end_headers()
                self.wfile.write(bytes("<html><head><title>ip-blocker</title></head>", "utf-8"))
                self.wfile.write(bytes("<p>404 Not Found</p>", "utf-8"))
                self.wfile.write(bytes("<body>", "utf-8"))
                self.wfile.write(bytes("</body></html>", "utf-8"))


if __name__ == "__main__":

    try:
        hostName = os.environ['HOSTNAME']
        serverPort = int(os.environ['SERVERPORT'])
        dbName = os.environ['DBNAME']
        dbUser = os.environ['DBUSER']
        dbHost = os.environ['DBHOST']
        dbPort = os.environ['DBPORT']
        dbPassword = os.environ['DBPASS']
        smtpserver = os.environ['SMTPSERVER']
        smtpport = os.environ['SMTPPORT']
        emailfrom = os.environ['EMAILFROM']
        emailto = os.environ['EMAILTO']
    except:
        print("Failed to get sys env.")
        sys.exit(1)

    try:
        con = psycopg2.connect(
            database=dbName,
            user=dbUser,
            password=dbPassword,
            host=dbHost,
            port=dbPort
        )
        print("Database opened successfully")
        cur = con.cursor()
    except:
        print("Failed to connect to pgsql")
        print("dbName: " + dbName)
        print("dbUser: " + dbUser)
        print("dbHost: " + dbHost)
        sys.exit(1)
    try:
        cur.execute('''CREATE TABLE IF NOT EXISTS IPBLOCKED  
             (DATE TIMESTAMPTZ NOT NULL,
             IP VARCHAR(39) NOT NULL,
             PATH VARCHAR(2000) NOT NULL);''')
        print("Table created successfully")
        con.commit()
    except:
        print("Failed to create table")

    blacklist = list()
    try:
        cur.execute("SELECT ip from IPBLOCKED")
        rows = cur.fetchall()
        for row in rows:
            blacklist.append(row[0])
    except:
        print("Failed to select ip from sql")

    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    con.close()
    print("Server stopped.")
