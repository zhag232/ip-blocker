# ip-blocker



## About project

This test job is called ip-blocker. This is repo for development, testing and deploy to my enviroment.
- If you looking for helm chart use this [link](https://gitlab.com/zhag232/ip-blocker-helm)
- If you try it, use this example
```
curl -v -H "Host: ip-blocker.zhag.com" "193.42.112.20/?n=10"
curl -v -H "Host: ip-blocker.zhag.com" "193.42.112.20/blacklisted"
```

## Feathures

- Database Master and Read Replica based on PostgreSQL.
- A simple python application with functions:
    - It responds to the URL like 'http://host/?n=x' and returns n*n.
    - It responds to the URL 'http://host/blacklisted' with conditions:
    - return error code 444 to the visitor
    - block the IP of the visitor
    - send an email with IP address
    - insert into PostgreSQL table information: path, IP address of the visitor and datetime when he got blocked
