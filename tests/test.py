import os

import pytest
import requests
import psycopg2

url = os.environ['APPURL']
myip = os.environ['IP']


@pytest.mark.order(1)
def test_get_404():
    response = requests.get("http://" + url + "/123")
    assert response.status_code == 404


@pytest.mark.order(2)
def test_get_500():
    response = requests.get("http://" + url + "/?n=x")
    assert response.status_code == 500


@pytest.mark.order(3)
def test_get_number():
    response = requests.get("http://" + url + "/?n=100")
    assert response.status_code == 200
    assert response.content == b'<html><head><title>ip-blocker</title></head><p>10000.0</p><body></body></html>'


@pytest.mark.order(4)
def test_get_blacklisted():
    response = requests.get("http://" + url + "/blacklisted")
    assert response.status_code == 444
    dbName = os.environ['DBNAME']
    dbUser = os.environ['DBUSER']
    dbPassword = os.environ['DBPASS']
    dbHost = os.environ['DBHOST']
    dbPort = os.environ['DBPORT']
    con = psycopg2.connect(
        database=dbName,
        user=dbUser,
        password=dbPassword,
        host=dbHost,
        port=dbPort
    )
    cur = con.cursor()
    cur.execute("SELECT ip from IPBLOCKED")
    rows = cur.fetchall()
    assert rows[0][0] == myip


@pytest.mark.order(5)
def test_am_i_blocked():
    response = requests.get("http://" + url + "/?n=100")
    assert response.status_code == 444
